---
split: true
code:
  - json
---

# 📆 Schedule
You can request the schedule form Kronos by using the following parameters:

## Required to request 
**URL**: ``https://pb-kronos.dev/api/schedule/<division>``  
**Required Parameters**:   
- ``<divisions>``:  
    - **PBST**  
    - **TMS**  
    - **PET**  
    - **PBM**  

**Required Method**: ``GET``  
**Required Headers**:    
```json
Headers = {
  ['Access-Key'] = "<YOUR TOKEN HERE>"
}
```

## Expected Response 
```json
{
"event-id-here": {
  "Duration":TIME_IN_MINUTES,
  "EventType":EVENT_TYPE,
  "Time":EVENT TIME IN UTC TIMESTAMPS,
  "Notes":EVENT_NOTES,
  "Trainer":HOST,
  "TrainingID":SAME_AS_EVENT_ID
}, 
"event-id-here": {
  ...
}, 
  ...
}
```

## Example Response 
```json
{
   "1c11f578-748c-4b45-88a6-ce77bb202f3f": {
      "Duration":60,
      "EventType":"Training",
      "Time":1608688800,
      "Notes":"None",
      "Trainer":"CombatSwift",
      "TrainingID":"1c11f578-748c-4b45-88a6-ce77bb202f3f"
   },
   "5c12f578-838c-4b45-8s66-ce477bg802f3f": {
      "Duration":120,
      "EventType":"Mass Patrol",
      "Time":1908688800,
      "Notes":"Random Time and Duration",
      "Trainer":"superstefano4",
      "TrainingID":"5c12f578-838c-4b45-8s66-ce477bg802f3f"
   }
}
```  


## Possible Error Messages
**Error Messages**:  
``403``: *Invalid Permission*  
``429``: *Exceeded ratelimit*  
``502``: *Schedule has not loaded for this division, try again later*  